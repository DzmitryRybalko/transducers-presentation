const randomInteger = (min, max) => {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  rand = Math.round(rand);

  return rand;
}

const cardsCount = 100000;
const cards = [...Array(cardsCount).keys()];

let transactions = [];
cards.forEach(card => {
  const transactionsCount = randomInteger(10, 50);
  for (let i = -1; ++i < transactionsCount;) {
    let transaction = {
      from: i,
      to: randomInteger(0, cardsCount - 1),
      count: randomInteger(1, 100)
    };

    transactions.push(transaction);
  }
});

module.exports.transactions = transactions;
