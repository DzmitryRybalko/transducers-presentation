const transduceJS = require('./transducers.js');
const transactions = require('./transactions.js').transactions;
const t = require('transducers-js');
const _ = require('lodash');

const compose = transduceJS.compose;
const filter = transduceJS.filter;
const map = transduceJS.map;
const transduce = transduceJS.transduce;


const mapTransactions = transaction => transaction;
const filterTransactions = transaction => transaction.from % 5;
const sumTransactions = (sum, transaction) => (sum += transaction.count, sum);

console.log('transaction: ', transactions[0], 'Lenghth: ', transactions.length);


let startTime = Date.now();
transactions
  .map(mapTransactions)
  .filter(filterTransactions)
  .reduce(sumTransactions, 0);

console.log('Time [map functions]: ', Date.now() - startTime);


startTime = Date.now();
sumF(filterF(transformF(transactions)));
console.log('Time [functions]: ', Date.now() - startTime);

startTime = Date.now();
calcSum(transactions);
console.log('Time [for]: ', Date.now() - startTime);


startTime = Date.now();
transduce(compose(
  map(mapTransactions),
  filter(filterTransactions)
), sumTransactions, 0, transactions);

console.log('Time [transducers]: ', Date.now() - startTime);


startTime = Date.now();
let xf = t.comp(t.map(mapTransactions), t.filter(filterTransactions));
t.transduce(xf, sumTransactions, 0, transactions);
console.log('Time [transducers Lib]: ', Date.now() - startTime);


startTime = Date.now();
_.reduce(
  _.filter(
    _.map(transactions, mapTransactions),
    filterTransactions
  ),
  sumTransactions,
  0
);
console.log('Time [Lodash]: ', Date.now() - startTime);

startTime = Date.now();
_(transactions)
  .map(mapTransactions)
  .filter(filterTransactions)
  .reduce(sumTransactions, 0);

console.log('Time [Lodash Lazy]: ', Date.now() - startTime);



function transformF(arr) {
  let result = [];
  for (let i = -1; ++i < arr.length;) {
    result.push(arr[i]);
  }

  return result;
}

function filterF(arr) {
  let result = [];
  for (let i = -1; ++i < arr.length;) {
    if (arr[i].from % 5) result.push(arr[i]);
  }

  return result;
}

function sumF(arr) {
  let result = 0;
  for (let i = -1; ++i < arr.length;) {
    result += arr[i].count;
  }

  return result;
}

function calcSum(arr) {
  let result = 0;
  for (let i = -1; ++i < arr.length;) {
    if (arr[i].from % 5) result += arr[i].count;
  }

  return result;
}
