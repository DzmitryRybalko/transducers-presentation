const reduce = (reducer, seed, iterable) => {
  let accumulation = seed;

  for (var value of iterable) {
    accumulation = reducer(accumulation, value);
  }

  return accumulation;
}

const map =
  (fn) =>
  (reducer) =>
  (acc, val) => reducer(acc, fn(val));

const filter =
  (filterFn) =>
  (reducer) =>
  (acc, val) => {
    return filterFn(val) ? reducer(acc, val) : acc;
  };

const compositionOf = (acc, val) => (...args) => acc(val(...args));
const compose = (...fns) => reduce(compositionOf, x => x, fns);

const transduce = (transformer, reducer, seed, iterable) => {
  const transformedReducer = transformer(reducer);

  return reduce(transformedReducer, seed, iterable);
}

module.exports.reduce = reduce;
module.exports.map = map;
module.exports.filter = filter;
module.exports.compose = compose;
module.exports.transduce = transduce;
